package com.tl.mav.rest.component;

import com.tl.mav.numbers.NumbersService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/num")
@RequiredArgsConstructor
public class NumbersController {

  private final NumbersService numbersService;

  @GetMapping("/fibonacci/{count}")
  public long[] get(@PathVariable("count") int count) {
    return numbersService.getFibonacci(count);
  }
}