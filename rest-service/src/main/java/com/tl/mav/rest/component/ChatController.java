package com.tl.mav.rest.component;

import com.tl.mav.chat.ChatService;
import com.tl.mav.chat.model.ChatStats;
import com.tl.mav.chat.model.StatusInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/chat")
@RequiredArgsConstructor
public class ChatController {

  private final ChatService chatService;

  @GetMapping("")
  public ChatStats get() {
    return chatService.activeUsers();
  }

  @GetMapping("/{name}/connect")
  public StatusInfo start(@PathVariable String name) {
    return chatService.connect(name);
  }

  @GetMapping("/{name}/status")
  public StatusInfo stop(@PathVariable String name) {
    return chatService.status(name);
  }

  @GetMapping("/{name}/disconnect")
  public StatusInfo status(@PathVariable String name) {
    return chatService.disconnect(name);
  }

}
