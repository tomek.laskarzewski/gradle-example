package com.tl.mav.numbers;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class IllegalNumberException extends ResponseStatusException {
  public IllegalNumberException() {
    super(
        HttpStatus.BAD_REQUEST,
        String.format("Number must be positive integer."));
  }
}
