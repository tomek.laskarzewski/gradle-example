package com.tl.mav.numbers;

public class NumbersService {

  public long[] getFibonacci(int count) {
    if (count < 0) {
      throw new IllegalNumberException();
    }
    long[] numbers = new long[count];
    for (int i = 0; i < count; i++) {
      numbers[i] = i < 2 ? i : numbers[i-2] + numbers[i-1];
    }
    return numbers;
  }


}
