package com.tl.mav.numbers;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class NumbersAutoConfiguration {

  @Bean
  @ConditionalOnMissingBean
  public NumbersService numbersService() {
    return new NumbersService();
  }
}
