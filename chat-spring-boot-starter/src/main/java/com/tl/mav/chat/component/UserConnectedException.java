package com.tl.mav.chat.component;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class UserConnectedException extends ResponseStatusException {
  public UserConnectedException(String name, long time) {
    super(
        HttpStatus.BAD_REQUEST,
        String.format("The user '%s' has already been connected for %d seconds.", name, time));
  }
}
