package com.tl.mav.chat.model;

public enum Status {
  CONNECTED, DISCONNECTED
}
