package com.tl.mav.chat;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class ChatAutoConfiguration {

  @Bean
  @ConditionalOnMissingBean
  public ChatService chatService() {
    return new ChatService();
  }
}
